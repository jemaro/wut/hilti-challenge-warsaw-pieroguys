#!/usr/bin/env python3
import enum
import time
from typing import List, Dict, Optional

from time import sleep
from pathlib import Path
from signal import SIGINT
import threading 
from subprocess import PIPE, Popen, run
from dotenv import load_dotenv

import os
import argparse

load_dotenv()

CWD = Path(os.path.dirname(os.path.abspath(__file__)))
ROS_PACKAGE_PATH = CWD / 'DBoW2_train'
print('CWD: ' + str(CWD))
ALL_METHODS = [
    f.name for f in ROS_PACKAGE_PATH.iterdir() if f.is_file() and not f.suffix
]
assert ALL_METHODS, 'HILTI SLAM is not compiled'
ENV = os.environ.copy()
ENV['ROS_PACKAGE_PATH'] = ROS_PACKAGE_PATH
TEST_DATA = CWD / 'test.bag'
DELAY = 0.1


def _print_stdout(process, break_on: Optional[str] = None):
    for thread in threading.enumerate(): 
        print(thread.name)
    for line in process.stdout:
        print(line, end='')
        if break_on and break_on in line:
            break
    return True


def get_vocabulary(
    data: List[Path] = [],
):
    # Open a process with roscore, then iterate over the datasets to be run
    # Ensure input is iterable
    if not isinstance(data, list):
        data = list(data)

    with Popen('roscore') as roscore:
        
        time.sleep(4)
        print(f'\nRoscore initialized with pid: {roscore.pid}')
        try:
            with Popen(
                [
                    'rosrun', 'DBoW2_train', 'dbow_train', str(DELAY)
                ],
                stdout=PIPE,
                text=True,
                cwd=CWD,
                ) as vocab_trainer:
                print(f'Vocab trainer initialized with pid: {vocab_trainer.pid}')
                try:
                    # Wait until SLAM node is initialized
                    # Keep printing stdout in a separate thread
                    printer = threading.Thread(target=_print_stdout, args=(vocab_trainer, ))
                    printer.start()
                    
                    # # Play the data and wait until it is finished
                    for d in data:
                        run_bag(d)
                        time.sleep(0.2)
                    print('Terminating vocab train node to save results')
                    vocab_trainer.send_signal(SIGINT)
                    vocab_trainer.wait()
                    printer.join()
                
                except Exception as e:
                    if not vocab_trainer.returncode:
                        vocab_trainer.terminate()
                    raise e
                if not roscore.poll():
                    roscore.terminate()
        # Ensure roscore is always terminated
        except Exception as e:
            if not roscore.returncode:
                roscore.terminate()
            raise e


def run_bag(data):
    print('Running bag: ' + str(data))
    run(f'rosbag play {data}', shell=True)

def get_data(datasets_path: Path) -> Dict[str, Path]:
    return {
        p.name: (p / 'data.bag')
        for p in datasets_path.iterdir()
        if p.is_dir() and (p / 'data.bag').exists()
    }


if __name__ == '__main__':
    DATASETS_PATH = os.environ.get('DATASETS_PATH', None)
    parser = argparse.ArgumentParser(
        description='Executes the trajectory estimator by opening ROS processes'
    )
    parser.add_argument(
        '-d',
        '--debug',
        dest='debug',
        action='store_true',
        help=f'If used it will run the test dataset {TEST_DATA}'
    )
    if DATASETS_PATH is not None:
        DATASETS_PATH = Path(DATASETS_PATH.replace(' ', "\ "))
        all_data = get_data(DATASETS_PATH)
        parser.add_argument(
            'datasets',
            default=list(range(len(all_data))),
            metavar='DATASET',
            nargs='*',
            help=(
                'Datasets to be used. Found in {DATASETS_PATH}. ' +
                'Numeric indexes or names can be used: ' + ', '.join([
                    str(i) + ': ' + n for i, n in enumerate(all_data.keys())
                ])
            )
        )
        kwargs = vars(parser.parse_args())
        _data = {}
        for k in kwargs.pop('datasets'):
            try:
                index = int(k)
                _data[list(all_data.values())[index]] = None
            except ValueError:
                if k in all_data:
                    _data[all_data[k]] = None
                else:
                    raise AttributeError(f'Invalid dataset name or index: {k}')
        data = list(_data.keys())
    else:
        parser.add_argument(
            '--datasets-path',
            type=Path,
            required=True,
            help='''Path to the Datasets folder. Required if DATASETS_PATH
            is not set as an environmental variable.'''
        )
        kwargs = vars(parser.parse_args())
        DATASETS_PATH = kwargs.pop('datasets_path')
        if not DATASETS_PATH or not DATASETS_PATH.is_dir():
            raise AttributeError(
                f'Invalid DATASETS_PATH, is not a folder: {DATASETS_PATH}'
            )
        data = list(get_data(DATASETS_PATH).values())

    if kwargs.pop('debug'):
        print("Debug mode")
        get_vocabulary([TEST_DATA], ** kwargs)
    elif not data:
        print(f'No dataset was found in {DATASETS_PATH}')
        exit()
    else:
        print("Training mode...")
        get_vocabulary(data, **kwargs)
