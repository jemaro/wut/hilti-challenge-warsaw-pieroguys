#!/bin/bash
cd "$(dirname "$0")"

# Troubleshot [rospack] Error: package 'HILTI_SLAM' not found
_path_append() {
    if [ -n "$2" ]; then
        case ":$(eval "echo \$$1"):" in
            *":$2:"*) :;;
            *) eval "export $1=\${$1:+\"\$$1:\"}$2" ;;
        esac
    else
        case ":$PATH:" in
            *":$1:"*) :;;
            *) export PATH="${PATH:+"$PATH:"}$1" ;;
        esac
    fi
}

_path_append ROS_PACKAGE_PATH "$(pwd)/HILTI_SLAM"

rosrun HILTI_SLAM mono \
    ./ORB_SLAM3/Vocabulary/ORBvoc.txt ./HILTI_SLAM/settings.yaml