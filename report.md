Request repository access to [Daniel Casado](dcasadoherraez@gmail.com) or [Andreu
Gimenez](esdandreu@gmail.com)

# Approach
This report reviews the approach for the implementation of visual SLAM using ORB-SLAM3 Monocular Inertial, as well as some custom notes and tested ideas to alter the methodology's performance in different environments. 

# Method description
ORB-SLAM3 was developed based on ORB-SLAM2 and ORB-SLAM, by researchers Carlos Campo et al. from University of Zaragoza. It is an optimization-based approach that allows the integration of visual-inertial odometry based on the extraction of image ORB features. The original paper can be found in [[1]](#1).

The code was implemented and tested modifying the set of descriptors in the vocabulary, trained for these particular datasets, and adjusted to its corresponding parameters. 

## System summary
![ORB-SLAM3 system by Carlos Campo et al.](https://gitlab.com/jemaro/wut/hilti-challenge-warsaw-pieroguys/-/raw/main/diagram.png)

Initialization starts by finding initial ORB correspondences, computes the homography and fundamentaly matrix so that the best one can be selected for planar and non-planar images, extracts motion hypotheses and performs initial BA.


Then, the systm is dicided into three threads, 1) tracking, 2) local mapping and 3) loop closing. However a new component to older ORB-SLAM algorithms is added, Atlas.
- Tracking: Localize the camera each frame and decide when to insert new keyframes to the map. If it gets lost, it will look for the scene in the previously saved Atlas maps, otherwise it will start a new one. This thread also deals with IMU in the optimization steps.
- Local mapping: Adds points to the map and performs local Bundle Adjustment.
- Loop closing: Every new keyframe loop closing is checked with the existing maps in Atlas. If found, pose-graph optimization is performed using Levenberg-Marquardt algorithm over an Essential Graph, containing all the keyframes as nodes, but less edges than a normal covisibility graph.
- Atlas: Contains the currently active map, and the non-active maps which have been saved after localization is lost. It also holds the database containing the ORB descriptor vocabulary found. 

## Optimization and Bundle adjustment based SLAM
### Loop closing
ORB-SLAM3 performs loop closing based on pose-graph optimization of an Essential Graph over similarity constraints (Sim3), built from a spanning tree maintained by the system, loop closure links and strong edges from the covisibility graph. Having limited links, keeping only the ones based on features from multiple keyframes improves the optimization procedure of the graph. 

### Motion-only BA
Regarding camera pose optimization, it is based on motion-only Bundle Adjustment. Motion only Bundle Adjustment (or pose optimization) is used optimizing only camera poses instead of points in the map. It has low computational cost by having a good initialization, and specific image keypoints and keyframes, avoiding using redundant frames. ORB features are fast to compute and match. Moreover they are stored in a hierarchical tree manner which enables very fast querying.

## Causal
ORB-SLAM3 is an online method that does not use any informtation from the future. 

## Sensor modalities
Sensors used for the final approach include KannalaBrandt monocular front camera from the alphasense sevensense head as well as  BMI085 IMU. 

## Total processing time for each sequence and the used hardware
The processing time for each sequence varies mainly on the CPU. We needed to slow down the rosbag play frequency to 0.5. Thus the duration for each sequence is 2*sequence_length(s)

It was executed on the following hardware:
```
Device name	Kenobi
Processor	Intel(R) Core(TM) i7-9750H CPU @ 2.60GHz   2.59 GHz
Installed RAM	16,0 GB (15,8 GB usable)
System type	64-bit operating system, x64-based processor
NVIDIA GeForce RTX 2060
```

## Parameters
The parameters used for all the sets where kept constant demonstrating the generalization of the ORB-SLAM3 algorithm for different environments. 
Specifications for the TUM Dataset with a similar IMU where used as a base, and the number of ORB features was varied with custom vocabulary sets.

## Other possible approaches
### ORB descriptor vocabulary training
ORB-SLAM3 is capable of performing efficient bundle adjustment based on the ORB descriptors it extracts from the images thanks to it's vocabulary database organization. This is formed by a vocabulary tree built using hierarchical quantization with k-means clustering as described in [[2]](#2), allowing for fast querying and retrieval from very large vocabulary databases. 

We trained the vocabulary dictionary using the DBoW2 library provided by the researchers [[3]](#3) with a branching factor of 10 and 6 layers. The input data was directly the competition rosbag image messages at a lower framerate. This vocabulary was then used with ORB-SLAM3 to carry out matching.

### RGB-D using LIDAR
Another option tested but not validated was to perform pointcloud projection to the camera image. Alignment was performed with help of the camera-imu tf transform, and the intrinsic camera matrix. 

However, as RGB-D requires a dense pointcloud, while LIDAR provided a sparse pointcloud, a solution would be to perform interpolation between points. The main problem here is the pointcloud density, being very sparse after checking the Ouster OS0-64 LIDAR. This would cause unprecise interpolation and trajectory tracking failure.

# References
<a id="1">[1]</a> 
Carlos Campos, Richard Elvira, Juan J. Gomez Rodriguez, Jose M.M. Montiel and Juan D. Tardos. 
"ORB-SLAM3: An Accurate Open-Source Library for Visual, Visual-Inertial and Multi-Map SLAM".
arXiv preprint arXiv:2007.11898 (2021). 

<a id="2">[2]</a> 
D. Nister and H. Stewenius, "Scalable Recognition with a Vocabulary Tree," 2006 IEEE Computer Society Conference on Computer Vision and Pattern Recognition (CVPR'06), 2006, pp. 2161-2168, doi: 10.1109/CVPR.2006.264.

<a id="3">[3]</a> 
Galvez-Lopez, Dorian and Tardos, J. D. "Bags of Binary Words for Fast Place Recognition in Image Sequences".
IEEE Transactions on Robotics (2012).
