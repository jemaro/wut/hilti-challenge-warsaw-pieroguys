#!/bin/bash
cd "$(dirname "$0")"

echo "Building ROS nodes"
mkdir build
cd build
cmake .. -DROS_BUILD_TYPE=Release
make -j
