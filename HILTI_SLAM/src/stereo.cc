/**
* This file is part of ORB-SLAM3
*
* Copyright (C) 2017-2020 Carlos Campos, Richard Elvira, Juan J. Gómez
* Rodríguez, José M.M. Montiel and Juan D. Tardós, University of Zaragoza.
* Copyright (C) 2014-2016 Raúl Mur-Artal, José M.M. Montiel and Juan D. Tardós,
* University of Zaragoza.
*
* ORB-SLAM3 is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation, either version 3 of the License, or (at your option) any later
* version.
*
* ORB-SLAM3 is distributed in the hope that it will be useful, but WITHOUT ANY
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
* A PARTICULAR PURPOSE. See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with
* ORB-SLAM3. If not, see <http://www.gnu.org/licenses/>.
*/


#include<iostream>
#include<algorithm>
#include<fstream>
#include<chrono>

#include<ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include<opencv2/core/core.hpp>

#include"../../ORB_SLAM3/include/System.h"

using namespace std;

class ImageGrabber {
public:
    ImageGrabber(ORB_SLAM3::System* pSLAM) :mpSLAM(pSLAM) {}

    void GrabStereo(
        const sensor_msgs::ImageConstPtr& msgLeft,
        const sensor_msgs::ImageConstPtr& msgRight);

    ORB_SLAM3::System* mpSLAM;
};

int main(int argc, char** argv) {
    // Process command line arguments
    std::string method = argv[0];
    std::string filename;
    bool viewer = true;

    method = method.substr(method.find_last_of("/\\") + 1, method.length());
    if (argc == 3) {
        std::time_t timestamp = std::time(nullptr);
        std::asctime(std::localtime(&timestamp));
        filename = std::to_string(timestamp) + "_" + method + ".txt";
    }
    else if (argc == 4) {
        filename = argv[3];
    }
    else if (argc == 5) {
        filename = argv[3];
        viewer = std::stoi(argv[4]);
    }
    else {
        cerr << endl << "Usage: rosrun HILTI_SLAM " << method
            << " path_to_vocabulary path_to_settings [output_path] [viewer]"
            << endl;
        ros::shutdown();
        return 1;
    }

    // Initialize ROS node
    ros::init(argc, argv, "RGBD");
    ros::start();

    // Create SLAM system. It initializes all system threads and gets ready to
    // process frames.
    ORB_SLAM3::System SLAM(
        argv[1], argv[2], ORB_SLAM3::System::STEREO, viewer);

    ImageGrabber igb(&SLAM);

    ros::NodeHandle nh;

    message_filters::Subscriber<sensor_msgs::Image> left_sub(
        nh, "alphasense/cam0/image_raw", 1);
    message_filters::Subscriber<sensor_msgs::Image> right_sub(
        nh, "alphasense/cam1/image_raw", 1);
    typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> sync_pol;
    message_filters::Synchronizer<sync_pol> sync(sync_pol(10), left_sub, right_sub);
    sync.registerCallback(boost::bind(&ImageGrabber::GrabStereo, &igb, _1, _2));

    ros::spin();

    // Stop all threads
    SLAM.Shutdown();

    // Save camera trajectory
    SLAM.SaveKeyFrameTrajectoryTUM(filename);
    // SLAM.SaveTrajectoryTUM("FrameTrajectory_TUM_Format.txt");

    ros::shutdown();

    return 0;
}

void ImageGrabber::GrabStereo(
    const sensor_msgs::ImageConstPtr& msgLeft,
    const sensor_msgs::ImageConstPtr& msgRight) {
    // Copy the ros image message to cv::Mat.
    cv_bridge::CvImageConstPtr cv_ptrLeft;
    try {
        cv_ptrLeft = cv_bridge::toCvShare(msgLeft);
    }
    catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    cv_bridge::CvImageConstPtr cv_ptrRight;
    try {
        cv_ptrRight = cv_bridge::toCvShare(msgRight);
    }
    catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }

    cv::Mat imLeft, imRight;
    cv::rotate(cv_ptrLeft->image, imLeft, cv::ROTATE_180);
    cv::resize(imLeft, imLeft, cv::Size(720, 540), 0, 0, cv::INTER_LINEAR);
    cv::resize(cv_ptrRight->image, imRight, cv::Size(720, 540), 0, 0, cv::INTER_LINEAR);
    mpSLAM->TrackStereo(imLeft, imRight, cv_ptrLeft->header.stamp.toSec());

}


