/**
* This file is part of ORB-SLAM3
*
* Copyright (C) 2017-2020 Carlos Campos, Richard Elvira, Juan J. Gómez Rodríguez, José M.M. Montiel and Juan D. Tardós, University of Zaragoza.
* Copyright (C) 2014-2016 Raúl Mur-Artal, José M.M. Montiel and Juan D. Tardós, University of Zaragoza.
*
* ORB-SLAM3 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with ORB-SLAM3.
* If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Imu.h>

#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>

#include <opencv2/core/core.hpp>

#include "../../ORB_SLAM3/include/System.h"
#include "../../ORB_SLAM3/include/ImuTypes.h"

using namespace std;

int main(int argc, char **argv)
{
    // Process command line arguments
    std::string method = argv[0];
    std::string filename;
    bool viewer = true;

    long int imu_buff;
    long int img_buff;

    bool bEqual = false;

    std::string source_frame;
    std::string target_frame;

    if (argc != 3)
    {
        cerr << endl
             << "Usage: HILTI_SLAM transform_listener [source_frame] [target_frame]"
             << endl;
        ros::shutdown();
        return 1;
    } else {
        source_frame = argv[1];
        target_frame = argv[2];
    }
    std::cout << "Use SIGINT to return the transform" << std::endl;

    // Initialize ROS node
    ros::init(argc, argv, "transform_listener");
    ros::start();

    geometry_msgs::TransformStamped transformStamped;
    tf2_ros::Buffer tfBuffer;

    tf2_ros::TransformListener tfListener(tfBuffer);

    ros::spin();

    try
    {
        std::cout << "Transform saved to "
                  << "imu_to_cam0.txt" << std::endl;
        transformStamped = tfBuffer.lookupTransform(target_frame, source_frame, ros::Time(0));

        // Declare what you need
        cv::FileStorage file("imu_to_cam0.txt", cv::FileStorage::WRITE);

        // Write to file!
        float q[4] = {transformStamped.transform.rotation.x,
                         transformStamped.transform.rotation.y,
                         transformStamped.transform.rotation.z,
                         transformStamped.transform.rotation.w};
        float t[3] = {transformStamped.transform.translation.x,
                          transformStamped.transform.translation.y,
                          transformStamped.transform.translation.z};

        float tau[16] = {1 - 2*q[1]*q[1] - 2*q[2]*q[2],     2*q[0]*q[1] + 2*q[3]*q[2],     2*q[0]*q[2] - 2*q[3]*q[1], t[0],
                             2*q[0]*q[1] - 2*q[3]*q[2], 1 - 2*q[0]*q[0] - 2*q[3]*q[3],     2*q[1]*q[2] + 2*q[3]*q[0], t[1],
                             2*q[0]*q[2] + 2*q[3]*q[1],     2*q[1]*q[2] - 2*q[3]*q[0], 1 - 2*q[0]*q[0] - 2*q[1]*q[1], t[2],
                             0.0                      ,     0.0                      ,     0.0                       , 1.0};
        
        cv::Mat trans_v = cv::Mat(4, 4, CV_32F, tau);
        std:: cout << "Transformation: " << source_frame << " to " << target_frame << endl;
        file << "tau" << trans_v;
    }
    catch (tf2::TransformException &ex)
    {
        ROS_WARN("%s", ex.what());
    }

    ros::shutdown();

    return 0;
}
