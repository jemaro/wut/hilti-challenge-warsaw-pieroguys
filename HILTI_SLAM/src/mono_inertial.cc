/**
* This file is part of ORB-SLAM3
*
* Copyright (C) 2017-2020 Carlos Campos, Richard Elvira, Juan J. Gómez Rodríguez, José M.M. Montiel and Juan D. Tardós, University of Zaragoza.
* Copyright (C) 2014-2016 Raúl Mur-Artal, José M.M. Montiel and Juan D. Tardós, University of Zaragoza.
*
* ORB-SLAM3 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM3 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
* the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with ORB-SLAM3.
* If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>
#include <vector>
#include <queue>
#include <thread>
#include <mutex>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/Imu.h>

#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>

#include <opencv2/core/core.hpp>

#include "../../ORB_SLAM3/include/System.h"
#include "../../ORB_SLAM3/include/ImuTypes.h"

using namespace std;

geometry_msgs::TransformStamped transformStamped;
tf2_ros::Buffer tfBuffer;

class ImuGrabber {
public:
    ImuGrabber() {};
    void GrabImu(const sensor_msgs::ImuConstPtr& imu_msg);

    queue<sensor_msgs::ImuConstPtr> imuBuf;
    std::mutex mBufMutex;
};

class ImageGrabber {
public:
    ImageGrabber(ORB_SLAM3::System* pSLAM, ImuGrabber* pImuGb, const bool bClahe) : mpSLAM(pSLAM), mpImuGb(pImuGb), mbClahe(bClahe) {}

    void GrabImage(const sensor_msgs::ImageConstPtr& msg);
    cv::Mat GetImage(const sensor_msgs::ImageConstPtr& img_msg);
    void SyncWithImu();

    queue<sensor_msgs::ImageConstPtr> img0Buf;
    std::mutex mBufMutex;

    ORB_SLAM3::System* mpSLAM;
    ImuGrabber* mpImuGb;

    const bool mbClahe;
    cv::Ptr<cv::CLAHE> mClahe = cv::createCLAHE(3.0, cv::Size(8, 8));
};

int main(int argc, char** argv) {
    // Process command line arguments
    std::string method = argv[0];
    std::string filename;
    bool viewer = true;
    long int imu_buff = 1000;
    long int img_buff = 100;
    bool bEqual = false;

    method = method.substr(method.find_last_of("/\\") + 1, method.length());
    if (argc == 3) {
        std::time_t timestamp = std::time(nullptr);
        std::asctime(std::localtime(&timestamp));
        filename = std::to_string(timestamp) + "_" + method + ".txt";
    }
    else if (argc == 4) {
        filename = argv[3];
    }
    else if (argc == 5) {
        filename = argv[3];
        viewer = std::stoi(argv[4]);
    }
    else if (argc == 6) {
        filename = argv[3];
        viewer = std::stoi(argv[4]);
        imu_buff = (long int)argv[5];
    }
    else if (argc == 7) {
        filename = argv[3];
        viewer = std::stoi(argv[4]);
        imu_buff = (long int)argv[5];
        img_buff = (long int)argv[6];
    }
    else if (argc == 8) {
        filename = argv[3];
        viewer = std::stoi(argv[4]);
        imu_buff = (long int)argv[5];
        img_buff = (long int)argv[6];
        bEqual = std::stoi(argv[7]);
    }
    else {
        cerr << endl
            << "Usage: rosrun HILTI_SLAM " << method
            << " path_to_vocabulary path_to_settings [output_path] [viewer] "
            << "[apply_regularization] [imu_buffer] [image_buffer]"
            << endl;
        ros::shutdown();
        return 1;
    }

    // Initialize ROS node
    ros::init(argc, argv, "Mono_Inertial");
    ros::start();

    // Create SLAM system. It initializes all system threads and gets ready to process frames.
    ORB_SLAM3::System SLAM(argv[1], argv[2], ORB_SLAM3::System::IMU_MONOCULAR, viewer);

    ImuGrabber imugb;
    ImageGrabber igb(&SLAM, &imugb, bEqual); // TODO

    ros::NodeHandle n("~");
    ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info);

    // Maximum delay, 5 seconds
    ros::Subscriber sub_imu = n.subscribe("/alphasense/imu", imu_buff, &ImuGrabber::GrabImu, &imugb);
    ros::Subscriber sub_img0 = n.subscribe("/alphasense/cam0/image_raw", img_buff, &ImageGrabber::GrabImage, &igb);

    tf2_ros::TransformListener tfListener(tfBuffer);

    std::thread sync_thread(&ImageGrabber::SyncWithImu, &igb);
    ros::spin();

    try {
        geometry_msgs::TransformStamped transformStamped;

        std::cout << "Getting transformStamped" << std::endl;
        transformStamped = tfBuffer.lookupTransform("cam0", "imu",
            ros::Time(0));

        // Declare what you need
        std::cout << "X: " << transformStamped.transform.translation.x << std::endl;
        cv::FileStorage file("imu_to_cam0.txt", cv::FileStorage::WRITE);

        // Write to file!
        float quat[4] = { transformStamped.transform.rotation.x,
                         transformStamped.transform.rotation.y,
                         transformStamped.transform.rotation.z,
                         transformStamped.transform.rotation.w };

        cv::Mat quat_v = cv::Mat(1, 4, CV_32F, quat);
        float trans[3] = { transformStamped.transform.translation.x,
                          transformStamped.transform.translation.y,
                          transformStamped.transform.translation.z };
        cv::Mat trans_v = cv::Mat(1, 3, CV_32F, trans);

        file << "imu_to_cam0_rot" << quat_v;
        file << "imu_to_cam0_trans" << trans_v;
    }
    catch (tf2::TransformException& ex) {
        ROS_WARN("%s", ex.what());
    }

    // Stop all threads
    SLAM.Shutdown();

    // Save camera trajectory
    SLAM.SaveKeyFrameTrajectoryTUM(filename);

    ros::shutdown();

    return 0;
}

void ImageGrabber::GrabImage(const sensor_msgs::ImageConstPtr& msg) {
    mBufMutex.lock();
    if (!img0Buf.empty())
        img0Buf.pop();
    img0Buf.push(msg);
    mBufMutex.unlock();
}

cv::Mat ImageGrabber::GetImage(const sensor_msgs::ImageConstPtr& img_msg) {
    // Copy the ros image message to cv::Mat.
    cv_bridge::CvImageConstPtr cv_ptr;
    cv::Mat rescaled_image;

    try {
        cv_ptr = cv_bridge::toCvShare(img_msg, sensor_msgs::image_encodings::MONO8);
        cv::Mat dImg = cv_ptr->image;
        int width = 720;
        int height = 540;

        cv::resize(dImg, rescaled_image, cv::Size(width, height));
    }
    catch (cv_bridge::Exception& e) {
        ROS_ERROR("cv_bridge exception: %s", e.what());
    }

    if (rescaled_image.type() == 0) {
        return rescaled_image.clone();
    }
    else {
        std::cout << "Error type" << std::endl;
        return rescaled_image.clone();
    }
}

void ImageGrabber::SyncWithImu() {
    while (1) {
        cv::Mat im;
        double tIm = 0;
        if (!img0Buf.empty() && !mpImuGb->imuBuf.empty()) {
            tIm = img0Buf.front()->header.stamp.toSec();
            if (tIm > mpImuGb->imuBuf.back()->header.stamp.toSec())
                continue;
            {
                this->mBufMutex.lock();
                im = GetImage(img0Buf.front());
                img0Buf.pop();
                this->mBufMutex.unlock();
            }

            vector<ORB_SLAM3::IMU::Point> vImuMeas;
            mpImuGb->mBufMutex.lock();
            if (!mpImuGb->imuBuf.empty()) {
                // Load imu measurements from buffer
                vImuMeas.clear();
                while (!mpImuGb->imuBuf.empty() && mpImuGb->imuBuf.front()->header.stamp.toSec() <= tIm) {
                    double t = mpImuGb->imuBuf.front()->header.stamp.toSec();
                    cv::Point3f acc(mpImuGb->imuBuf.front()->linear_acceleration.x, mpImuGb->imuBuf.front()->linear_acceleration.y, mpImuGb->imuBuf.front()->linear_acceleration.z);
                    cv::Point3f gyr(mpImuGb->imuBuf.front()->angular_velocity.x, mpImuGb->imuBuf.front()->angular_velocity.y, mpImuGb->imuBuf.front()->angular_velocity.z);
                    vImuMeas.push_back(ORB_SLAM3::IMU::Point(acc, gyr, t));
                    mpImuGb->imuBuf.pop();
                }
            }
            mpImuGb->mBufMutex.unlock();
            if (mbClahe)
                mClahe->apply(im, im);

            mpSLAM->TrackMonocular(im, tIm, vImuMeas);
        }

        std::chrono::milliseconds tSleep(1);
        std::this_thread::sleep_for(tSleep);
    }
}

void ImuGrabber::GrabImu(const sensor_msgs::ImuConstPtr& imu_msg) {
    mBufMutex.lock();
    imuBuf.push(imu_msg);
    mBufMutex.unlock();
    return;
}
