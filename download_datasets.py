from typing import Union, List
from pathlib import Path
#from dotenv import load_dotenv

import os
import shutil
import argparse
import requests
import functools
from tqdm.auto import tqdm

#load_dotenv()

BASE_URL = 'https://storage.googleapis.com/hilti_challenge/'

def yes_or_no(question):
    reply = str(input(question + ' (y/n): ')).lower().strip()
    if reply[0] == 'y':
        return True
    if reply[0] == 'n':
        return False
    else:
        return yes_or_no("Uhhhh... please enter ")


DATASETS_PATH = os.getenv('DATASETS_PATH')
if not DATASETS_PATH:
    DATASETS_PATH = Path(
        os.path.dirname(os.path.abspath(__file__))
        ) / 'Datasets'
    if not yes_or_no(
        'DATASETS_PATH is not set as environmental variable, do you want to '
        f'continue downloading to: {DATASETS_PATH}'
        ):
        exit()
else:
    DATASETS_PATH = Path(DATASETS_PATH)


DATASETS = {
    'RPG Drone Testing Arena': {
        'path': 'uzh_tracking_area_run2',
        '6DOF': '_imu'
        },
    'IC Office': {
        'path': 'IC_Office_1'
        },
    'Office Mitte': {
        'path': 'Office_Mitte_1'
        },
    'Parking Deck': {
        'path': 'Parking_1'
        },
    'Basement': {
        'path': 'Basement_1',
        '3DOF': '_pole'
        },
    'Basement 3': {
        'path': 'Basement_3'
        },
    'Basement 4': {
        'path': 'Basement_4',
        '6DOF': '_prism'
        },
    'Lab': {
        'path': 'LAB_Survey_2'
        },
    'Construction Site Outdoor 1': {
        'path': 'Construction_Site_1'
        },
    'Construction Site Outdoor 2': {
        'path': 'Construction_Site_2',
        '3DOF': '_prism'
        },
    'Campus 1': {
        'path': 'Campus_1'
        },
    'Campus 2': {
        'path': 'Campus_2',
        '3DOF': '_prism'
        },
    }

def download_file(url, path):
    
    r = requests.get(url, stream=True, allow_redirects=True)
    if r.status_code != 200:
        r.raise_for_status()  # Will only raise for 4xx codes, so...
        raise RuntimeError(f"Request to {url} returned status code {r.status_code}")
    file_size = int(r.headers.get('Content-Length', 0))

    path.parent.mkdir(parents=True, exist_ok=True)

    desc = f'{path.parent.name} {path.name}'
    if file_size == 0:
        desc += '(Unknown total file size)'
    r.raw.read = functools.partial(r.raw.read, decode_content=True)  # Decompress if needed
    with tqdm.wrapattr(r.raw, "read", total=file_size, desc=desc) as r_raw:
        with path.open("wb") as f:
            shutil.copyfileobj(r_raw, f)

    return path


def download_dataset(name: str):
    dataset = DATASETS[name]
    slug = dataset['path']
    url = f'{BASE_URL}{slug}.bag'
    path = DATASETS_PATH / slug / 'data.bag'
    print(name)
    if path.exists():
        print(f'Found existing dataset at {path}')
    else:
        download_file(url, path)
    for truth in ['6DOF', '3DOF']:
        if truth in dataset:
            url = f'{BASE_URL}{slug}{dataset[truth]}.txt'
            path = DATASETS_PATH / slug / truth / 'groundtruth.txt'
            download_file(url, path)


def download_datasets(keys: List[Union[str, int]]):
    names = set()
    all_names = list(DATASETS.keys())
    for k in keys:
        try:
            index = int(k)
            names.add(all_names[index])
        except ValueError:
            if k in DATASETS:
                names.add(k)
            else:
                raise AttributeError(f'Invalid dataset name or index: {k}')
    for name in names:
        download_dataset(name)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Download HILTI SLAM Challenge datasets'
        )
    parser.add_argument(
        'datasets',
        default=list(range(len(DATASETS))),
        metavar='DATASET',
        nargs='*',
        help=(
            'Datasets to be downloaded. Numeric indexes or names can be used: '
            + ', '.join([
                str(i) + ': ' + n for i, n in enumerate(DATASETS.keys())
                ])
            )
        )

    args = parser.parse_args()
    download_datasets(args.datasets)
