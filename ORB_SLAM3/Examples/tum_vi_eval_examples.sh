#!/bin/bash
pathDatasetTUM_VI='/home/daniel/HILTI_SLAM_Challenge/hilti-challenge-warsaw-pieroguys/ORB_SLAM3/Datasets/TUM_VI' #Example, it is necesary to change it by the dataset path


# Single Session Example

echo "Launching Magistrale 1 with Stereo-Inertial sensor"
./Stereo-Inertial/stereo_inertial_tum_vi ../Vocabulary/ORBvoc.txt ./Stereo-Inertial/TUM_512.yaml "$pathDatasetTUM_VI"/dataset-corridor4_512_16/mav0/cam0/data "$pathDatasetTUM_VI"/dataset-corridor4_512_16/mav0/cam1/data ./Stereo-Inertial/TUM_TimeStamps/dataset-corridor4_512.txt ./Stereo-Inertial/TUM_IMU/dataset-corridor4_512.txt dataset-corridor4_512_stereoi
echo "------------------------------------"
echo "Evaluation of Magistrale 1 trajectory with Stereo-Inertial sensor"
python ../evaluation/evaluate_ate_scale.py "$pathDatasetTUM_VI"/corridor4_512_16/mav0/mocap0/data.csv f_dataset-corridor4_512_stereoi.txt --plot corridor4_512_stereoi.pdf
