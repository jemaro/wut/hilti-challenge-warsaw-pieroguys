#!/bin/bash
# Andreu Gimenez Bolinches esdandreu@gmail.com 16/09/2021

# Tested in:
# WSL Ubuntu 20.04

# The first argument is the number of processes to use on the setup build
nproc=${1}

apt-get update

apt-get install -y gnupg2 curl lsb-core vim wget \
    libpng16-16 libjpeg-turbo8 libtiff5


# Installing ROS
if [[ "$(lsb_release -sc)" == "focal" ]]; then
    ROS_VERSION="noetic"
elif [[ "$(lsb_release -sc)" == "bionic" ]]; then
    ROS_VERSION="melodic"
else
    echo "Not supported ubuntu version: $(lsb_release -sc)"
    exit 1
fi
sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
curl -sSL 'http://keyserver.ubuntu.com/pks/lookup?op=get&search=0xC1CF6E31E6BADE8868B172B4F42ED6FBAB17C654' | apt-key add -
apt update
apt install -y ros-$ROS_VERSION-desktop
# Append line to ~./bashrc if it is not there yet
grep -qxF "source /opt/ros/$ROS_VERSION/setup.bash" ~/.bashrc || \
    echo "source /opt/ros/$ROS_VERSION/setup.bash" >> ~/.bashrc
grep -qxF "export ROS_PACKAGE_PATH=\${ROS_PACKAGE_PATH}:$(pwd)/ORB_SLAM3/Examples/ROS" ~/.bashrc || \
    echo "export ROS_PACKAGE_PATH=\${ROS_PACKAGE_PATH}:$(pwd)/ORB_SLAM3/Examples/ROS" >> ~/.bashrc
grep -qxF "export ROS_PACKAGE_PATH=\${ROS_PACKAGE_PATH}:$(pwd)/HILTI_SLAM" ~/.bashrc || \
    echo "export ROS_PACKAGE_PATH=\${ROS_PACKAGE_PATH}:$(pwd)/HILTI_SLAM" >> ~/.bashrc
source ~/.bashrc
# Dependencies for building packages
apt-get install -y python3-rosdep
rosdep init
rosdep update
apt-get install -y python3-rosinstall python3-rosinstall-generator \
    python3-wstool python3-rospkg
apt-get install -y python3-catkin-tools
apt-get install -y software-properties-common
grep -qxF "export PYTHONPATH=\${PYTHONPATH}:/usr/lib/python3/dist-packages" ~/.bashrc || \
    echo "export PYTHONPATH=\${PYTHONPATH}:/usr/lib/python3/dist-packages" >> ~/.bashrc
update-alternatives --install /usr/bin/python python /usr/bin/python3 1

# Base tools and dependencies
apt-get install -y \
        cmake build-essential git unzip pkg-config \
        libeigen3-dev
        # libpython2.7-dev \
apt-get install -y python3-pip

curl -fsSL https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
add-apt-repository "deb https://download.sublimetext.com/ apt/stable/"
apt update
apt install -y sublime-text

# Build OpenCV (3.0 or higher should be fine)
# Note: "sudo make install" is used in order to install the package globaly
apt-get install -y python3-dev python3-numpy python3-matplotlib
apt-get install -y python-dev python-numpy
apt-get install -y libavcodec-dev libavformat-dev libswscale-dev
apt-get install -y libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev
apt-get install -y libgtk-3-dev

OPENCV_VERSION='4.5.1'

cd /tmp
git clone https://github.com/opencv/opencv.git
cd opencv && git checkout $OPENCV_VERSION && \
    mkdir -p build && cd build && \
    cmake -D CMAKE_BUILD_TYPE=Release -D BUILD_EXAMPLES=OFF -D BUILD_DOCS=OFF \
        -D BUILD_PERF_TESTS=OFF -D BUILD_TESTS=OFF \
        -D CMAKE_INSTALL_PREFIX=/usr/local .. && \
    make -j$nproc && sudo make install && \
    cd / && sudo rm -rf /tmp/opencv

# Build Pangolin
# Note: "sudo make install" is used in order to install the package globaly
apt-get install -y libgl1-mesa-dev
apt-get install -y libglew-dev
apt-get install -y libboost-dev libboost-thread-dev libboost-filesystem-dev
apt-get install -y ffmpeg libavcodec-dev libavutil-dev \
    libavformat-dev libswscale-dev
apt-get install -y libpng-dev

cd /tmp 
git clone https://github.com/stevenlovegrove/Pangolin
cd Pangolin && git checkout v0.6 && \
    mkdir -p build && cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS=-std=c++11 .. && \
    make -j$nproc && sudo make install && \
    cd / && sudo rm -rf /tmp/Pangolin