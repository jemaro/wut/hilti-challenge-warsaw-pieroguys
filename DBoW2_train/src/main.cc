#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <cv_bridge/cv_bridge.h>
#include <iostream>
#include <vector>

// DBoW2
#include "DBoW2.h" // defines OrbVocabulary and OrbDatabase

// OpenCV
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/features2d.hpp>

using namespace DBoW2;
using namespace std;

void loadFeatures(vector<vector<cv::Mat>> &features);
void changeStructure(const cv::Mat &plain, vector<cv::Mat> &out);
void testVocCreation(const vector<vector<cv::Mat>> &features);
void testDatabase(const vector<vector<cv::Mat>> &features);

cv::Ptr<cv::ORB> orb = cv::ORB::create();
vector<vector<cv::Mat>> features;

// ----------------------------------------------------------------------------
void imageCallback(const sensor_msgs::ImageConstPtr &msg, double waiter)
{
    try
    {
        cv::imshow("view", cv_bridge::toCvShare(msg, "bgr8")->image);

        // cout << "Extracting ORB features..." << endl;

        cv::Mat image = cv_bridge::toCvShare(msg, "bgr8")->image;
        cv::Mat mask;
        vector<cv::KeyPoint> keypoints;
        cv::Mat descriptors;

        orb->detectAndCompute(image, mask, keypoints, descriptors);

        features.push_back(vector<cv::Mat>());
        changeStructure(descriptors, features.back());
        ros::Duration(waiter).sleep();
    }
    catch (cv_bridge::Exception &e)
    {
        ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }
}

void changeStructure(const cv::Mat &plain, vector<cv::Mat> &out)
{
    out.resize(plain.rows);

    for (int i = 0; i < plain.rows; ++i)
    {
        out[i] = plain.row(i);
    }
}

// ----------------------------------------------------------------------------

void testDatabase(const vector<vector<cv::Mat>> &features)
{
    cout << "Creating a small database..." << endl;

    // load the vocabulary from disk
    OrbVocabulary voc("small_voc.yml.gz");

    OrbDatabase db(voc, false, 0); // false = do not use direct index
    // (so ignore the last param)
    // The direct index is useful if we want to retrieve the features that
    // belong to some vocabulary node.
    // db creates a copy of the vocabulary, we may get rid of "voc" now

 
    // we can save the database. The created file includes the vocabulary
    // and the entries added
    cout << "Saving database..." << endl;
    db.save("small_db.yml.gz");
    cout << "... done!" << endl;

    // once saved, we can load it again
    cout << "Retrieving database once again..." << endl;
    OrbDatabase db2("small_db.yml.gz");
    cout << "... done! This is: " << endl
         << db2 << endl;
}

// ----------------------------------------------------------------------------

int main(int argc, char **argv)
{
    cout << "Creating vocabulary..." << endl;
    features.clear();
    ros::init(argc, argv, "image_listener");
    ros::NodeHandle nh;

    image_transport::ImageTransport it(nh);
    image_transport::Subscriber sub = it.subscribe("alphasense/cam1/image_raw", 1, boost::bind(imageCallback,_1, std::stod(argv[1])));
    ros::spin();

    const int k = 10;
    const int L = 6;
    const WeightingType weight = TF_IDF;
    const ScoringType scoring = L1_NORM;

    OrbVocabulary voc(k, L, weight, scoring);

    cout << "Creating a " << k << "^" << L << " vocabulary..." << endl;
    voc.create(features);
    cout << "... done!" << endl;

    cout << "Vocabulary information: " << endl
         << voc << endl
         << endl;

    // save the vocabulary to disk
    cout << endl
         << "Saving vocabulary..." << endl;
    voc.saveToTextFile("../small_voc.txt");
    cout << "Done" << endl;

    return 1;
}


