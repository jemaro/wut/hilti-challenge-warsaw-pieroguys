# How to create your own bag of words

Add to `.bashrc`:
`export ROS_PACKAGE_PATH=${ROS_PACKAGE_PATH}:/home/daniel/HILTI_SLAM_Challenge/hilti-challenge-warsaw-pieroguys/DBoW_Training/DBoW2_train`

Then run the node:
`rosrun DBoW2_train dbow_train [record delay (in seconds)]`

At the same time, play the rosbag:
`rosbag play <path to rosbag>/data.bag <published message>:=camera/image`

You can play one after another to make the full Vocabulary dictionary.

CTRL+C to stop the recording and wait until the .txt vocabulary file is saved

