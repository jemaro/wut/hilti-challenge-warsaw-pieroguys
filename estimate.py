#!/usr/bin/env python3
from typing import List, Dict, Optional

from time import sleep
from pathlib import Path
from signal import SIGINT
from threading import Thread
from subprocess import PIPE, Popen, run
from dotenv import load_dotenv

import os
import argparse

load_dotenv()

CWD = Path(os.path.dirname(os.path.abspath(__file__)))
ROS_PACKAGE_PATH = CWD / 'HILTI_SLAM'
ALL_METHODS = [
    f.name for f in ROS_PACKAGE_PATH.iterdir() if f.is_file() and not f.suffix
    ]
assert ALL_METHODS, 'HILTI SLAM is not compiled'
ENV = os.environ.copy()
ENV['ROS_PACKAGE_PATH'] = ROS_PACKAGE_PATH
VOCABULARY_PATH = CWD / 'ORB_SLAM3' / 'Vocabulary' / 'ORBvoc.txt'
# VOCABULARY_PATH = CWD / 'DBoW_Training' / 'DBoW2_train' / 'small_voc.txt'
SETTINGS_PATH = ROS_PACKAGE_PATH / 'settings.yaml'
TEST_DATA = CWD / 'test.bag'
assert VOCABULARY_PATH.exists(), \
    f'ORB Vocabulary not found at {VOCABULARY_PATH}'
assert SETTINGS_PATH.exists(), \
    f'System settings not found at {SETTINGS_PATH}'


def _print_stdout(process, break_on: Optional[str] = None):
    for line in process.stdout:
        print(line, end='')
        if break_on and break_on in line:
            break
    return True


def _estimate(
        data: Path = TEST_DATA,
        method: str = 'mono',
        output_file: Path = Path('results/test.txt'),
        viewer: bool = True,
    ):
    # Open a process with the SLAM node, then play the data
    with Popen(
        [
            '/opt/ros/noetic/bin/rosrun', 'HILTI_SLAM', method,
            VOCABULARY_PATH, SETTINGS_PATH, output_file,
            str(int(viewer))
            ],
        stdout=PIPE,
        text=True,
        cwd=CWD,
        env=ENV,
        ) as slam:
        print(f'SLAM node pid: {slam.pid}')
        try:
            # Wait until SLAM node is initialized
            _print_stdout(slam, break_on='Vocabulary loaded!')
            # Keep printing stdout in a separate thread
            printer = Thread(target=_print_stdout, args=(slam, ))
            printer.start()
            # Play the data and wait until it is finished
            run(f'rosbag play {"-q" if not viewer else ""} -r 0.5 {data}', shell=True)
            # Terminate SLAM node and printer thread
            print('Terminating SLAM node to save results')
            slam.send_signal(SIGINT)
            printer.join()
        # Ensure SLAM node is always terminated
        except Exception as e:
            if not slam.returncode:
                slam.terminate()
            raise e


def estimate(
        data: List[Path] = [],
        methods: List[str] = ['Mono'],
        output_dir: Path = Path('results'),
        viewer: bool = True,
    ):
    # Open a process with roscore, then iterate over the datasets to be run
    # Ensure input is iterable
    if not isinstance(data, list):
        data = list(data)
    if not isinstance(methods, list):
        methods = list(methods)
    with Popen('roscore') as roscore:
        print(f'roscore pid: {roscore.pid}')
        try:
            sleep(1)
            print('ROS initialized')
            for m in methods:
                for d in data:
                    print(f'Processing {d.parent.name}')
                    _estimate(
                        data=d,
                        method=m,
                        viewer=viewer,
                        output_file=output_dir /
                        f'{d.parent.name.upper()}_{m}.txt',
                        )
            if not roscore.poll():
                roscore.terminate()
        # Ensure roscore is always terminated
        except Exception as e:
            if not roscore.returncode:
                roscore.terminate()
            raise e


def get_data(datasets_path: Path) -> Dict[str, Path]:
    return {
        p.name: (p / 'data.bag')
        for p in datasets_path.iterdir()
        if p.is_dir() and (p / 'data.bag').exists()
        }


if __name__ == '__main__':
    DATASETS_PATH = os.environ.get('DATASETS_PATH', None)
    parser = argparse.ArgumentParser(
        description='Executes the trajectory estimator by opening ROS processes'
        )
    parser.add_argument(
        '-m',
        '--methods',
        nargs='+',
        default=ALL_METHODS,
        help=f'Methods to use for the estimation. Defaults to {ALL_METHODS}',
        )
    parser.add_argument(
        '-d',
        '--debug',
        dest='debug',
        action='store_true',
        help=f'If used it will run the test dataset {TEST_DATA}'
        )
    parser.add_argument(
        '--no-viewer',
        dest='viewer',
        action='store_false',
        help='Avoid using a viewer'
        )
    parser.add_argument(
        '-o',
        '--output-dir',
        default=CWD / 'results',
        help='Output directory where the trajectory estimatiosn will be saved'
        )
    if DATASETS_PATH is not None:
        DATASETS_PATH = Path(DATASETS_PATH.replace(' ', "\ "))
        all_data = get_data(DATASETS_PATH)
        parser.add_argument(
            'datasets',
            default=list(range(len(all_data))),
            metavar='DATASET',
            nargs='*',
            help=(
                'Datasets to be used. Found in {DATASETS_PATH}. ' +
                'Numeric indexes or names can be used: ' + ', '.join([
                    str(i) + ': ' + n for i, n in enumerate(all_data.keys())
                    ])
                )
            )
        kwargs = vars(parser.parse_args())
        _data = {}
        for k in kwargs.pop('datasets'):
            try:
                index = int(k)
                _data[list(all_data.values())[index]] = None
            except ValueError:
                if k in all_data:
                    _data[all_data[k]] = None
                else:
                    raise AttributeError(f'Invalid dataset name or index: {k}')
        data = list(_data.keys())
    else:
        parser.add_argument(
            '--datasets-path',
            type=Path,
            required=True,
            help='''Path to the Datasets folder. Required if DATASETS_PATH
            is not set as an environmental variable.'''
            )
        kwargs = vars(parser.parse_args())
        DATASETS_PATH = kwargs.pop('datasets_path')
        if not DATASETS_PATH or not DATASETS_PATH.is_dir():
            raise AttributeError(
                f'Invalid DATASETS_PATH, is not a folder: {DATASETS_PATH}'
                )
        data = list(get_data(DATASETS_PATH).values())

    kwargs['output_dir'].mkdir(parents=True, exist_ok=True)
    if kwargs.pop('debug'):
        estimate([TEST_DATA], **kwargs)
    elif not data:
        print(f'No dataset was found in {DATASETS_PATH}')
        exit()
    else:
        estimate(data, **kwargs)