# Hilti Challenge Warsaw Pieroguys

Hilti SLAM Challenge: https://hilti-challenge.com/

Datasets: https://hilti-challenge.com/dataset.html

Submission: https://hilti-challenge.com/evaluation.html

Github: https://github.com/hemi86/hiltislamchallenge


# ORB_SLAM3
Useful steps
https://github.com/Mauhing/ORB_SLAM3

## Notes
`./build_ros.sh` should not be executed with `sudo` as it uses user
environmental variable. Also `rosdep update` needs to be executed beforehand.

`ORB_SLAM3/src/CameraModels/KannalaBrandt8.cpp` and
`ORB_SLAM3/src/LocalMapping.cc` are [skipped for
changes](https://stackoverflow.com/questions/4348590/how-can-i-make-git-ignore-future-revisions-to-a-file)
because of the problems building across installations.

# ROS

Visualizing data with ros

```
rosbag play --pause V1_02_medium.bag /cam0/image_raw:=/camera/left/image_raw /cam1/image_raw:=/camera/right/image_raw /imu0:=/imu
```

# Dataset

## .env
```
# Path to your Datasets folder, could be an external disk, for example:
DATASETS_PATH='/mnt/f/Datasets'
```